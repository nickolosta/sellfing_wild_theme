License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
AccessPress Mag WordPress Theme, Copyright 2015 AccessPress Themes
AccessPress Mag is distributed under the terms of the GNU GPL v3


Install Steps:
--------------

1. Activate the theme
2. Go to the Theme Option page
3. Setup theme options


Activate Home Page
--------------------
1. Make a new page and assign a Home Template to it.
2. Go to Settings - Reading and Select static page for Front page display option.
3. Select the created page with Home Template in the Front page dropdown.
4. Set up the home page in Appearance - Theme Options


------------------------------------------

JS Files 
    bxSlider: WTFPL and MIT license
    https://github.com/stevenwanderski/bxslider-4
    
    WOW Min JS: MIT license
    https://github.com/matthieua/WOW
    
    Jquery News Ticker: GPL v2
    http://www.jquerynewsticker.com/
  
-------------------------------------------
Fonts
    Font Awesome: MIT and GPL licenses
    http://fontawesome.io/license/
    
    Oswald
    https://www.google.com/fonts/specimen/Oswald
    SIL Open Font License, 1.1
    
    Open Sans
    http://www.google.com/fonts/specimen/Open+Sans
    Apache License, version 2.0
    
    Dosis
    http://www.google.com/fonts/specimen/Dosis
    SIL Open Font License, 1.1

----------------------------------------------------

ScreenShot Images
    https://pixabay.com/en/glamour-style-hat-woman-portrait-678834/
    http://pixabay.com/en/model-fashion-glamour-girl-female-600238/
    https://pixabay.com/en/girls-friends-friendship-young-685778/
    https://pixabay.com/en/woman-beach-model-beauty-game-692785/

GPL compatible for own images
    accesspress-mag/images/demo-images/728-90.png
    accesspress-mag/images/demo-images/300-250.png

----------------------------------------------------
== Changelog ==
Version 2.0.7
 * Display full post of slider in mobile device
 * Fixed bugs on slider function and add extra slider function for mobile device
 * Fixed bugs about page sidebar for old posts
 * Changed theme package name (Accesspress Mag to AccessPress Mag) 

Version 2.0.6
 * Added news ticker option
 * Added few animation at home page banner
 * Refine some code at accesspress-function.php file

Version 2.0.5
 * Fixed the error for widget title
 
Version 2.0.4
 * Added credit link about screenshot images.
 
Version 2.0.3
 * Removed unused demo files.
 * Removed unused demo images.

Version 2.0.2
 * Removed unused file from widget folder.
 * Fixed required option in TGMPA.
 * Remove menu off/on option from theme option.
 
Version 2.0.1
 * Removed demo content from theme
 * Fixed pagination in home page(index.php)
 * Managed custom background setting, header logo and header text from customizer.
 * Removed menu selection from theme option and managed from core.
 * Fixed copyright as reviewr.
 * Updated version of TGMPA.
 * Managed enqueue function about google fonts.
 * Removed prefix from third party resources.
 * Minor changes in theme option panel.
 * Fixed some bugs in css file.


Version 2.0
 * Fixed spelling of "publish" in homepage.
 * Removed widget-latest-reviews.php file from widgets folder.
 * Fixed all bugs in archive page and single page of product (woocommerce).
 * Add required woocommerce template.
 * Changed primary menu class name in header.php
 * Fixed slider on ipad screen.
 * Show search icon in mobile version.
 * Responsive for theme option panel.  

Version 1.1.9
 * Fixed bugs on sidebar-home
 * Changed option of TGM ('dismissable').
 * Removed unusal option (show/hide breadcrumbs on single page).
 * Changed image size while crop.
 * Changed image size of demo content.

Version 1.1.8
 * Fixed bug on homepage sidebar
 * Fixed bug on excerpt area at homepage
 
Version 1.1.7
 * Fixed sidebar option for homepage.
 * Fixed bug (removed esc_url at user avatar) on Article contributor widget.
 * Added css for gallery inside post.

Version 1.1.6
 * Added demo content for advertisement area in header.php , sidebar-home.php and home-page.php
 * Changed status of �force-activation� in accesspress-function.php	
 * Fixed id of �header ad widget� in function.php

Version 1.1.5
 * Added demo and support url

Version 1.1.4
 * Changed theme description
 * Fixed demo content

Version 1.0.1
* Added latest posts widget
* Added default no-image available
* Fixed some bugs

Version 1.0.0
* Submitted theme for review in http://wordpress.org