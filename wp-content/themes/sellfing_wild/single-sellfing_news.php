<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				$id = get_the_ID();
				$post = get_post($id);
				if(have_posts()){
					while(have_posts()): the_post();
						if ( is_single() ){
							the_content();
						}
					endwhile;
				};
			?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();
