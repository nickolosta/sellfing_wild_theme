<?php
/**
 * The Header template
 *
 * @package WordPress
 * @subpackage Sellfing One
 * @since Sellfing One 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/blocks/slider.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/blocks/captions.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/blocks/feedback.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page"  class="clearfix">
		<header id="site_header">
			<a id="logo_cont" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<span id="site-title"><?php bloginfo( 'name' ); ?></span><br>
			</a>
			<nav id="menu_top_cont">
				<?php wp_nav_menu(['theme_location' => 'primary','menu'=>'top menu','menu_class' => 'nav-menu', 'menu_id' => 'primary-menu'])?>
			</nav>
		</header>
		<?php
			if(!is_page( 'Home' )){?>
				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
					<?php if(function_exists('bcn_display'))
					{
						bcn_display();
					}?>
				</div>
			<?php }
		?>
		<div id="main">
