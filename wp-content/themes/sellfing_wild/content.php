<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if(!is_page( 'Home' )){
				the_title( '<h1 class="title_page">', '</h1>' );
		}
		?>
	</header><!-- .entry-header -->
		<?php
			/* translators: %s: Name of current post */
			if(!is_page('Home')){
				echo '<div id="about_page_content_cont"><div id="about_page_content">';
					the_content();
				echo '</div></div>';
			}else{
				the_content();
			}
			if(is_page( 'Home' )){
				echo '<div id="rootpage_map_cont"><div id="rootpage_map"></div></div>';
			}

		?>

</article><!-- #post-## -->
