
/**
 * Created by nick on 17.09.15.
 */
var map;
var initMap=function() {
        map = new google.maps.Map(document.getElementById('rootpage_map'), {
            center: {lat:40.679812, lng:-73.945146},
            zoom: 15,
            scrollwheel: false
        });
};
$(document).ready(function(){
    var main ={
        init:function(){
            var $bxSlider = $('.bxslider');
            var speed =$bxSlider.attr('data-speed');
            var interval =$bxSlider.attr('data-interval');
            var width =$('.slider_container').width();
            console.log(width);
            if(typeof $.fn.bxSlider!='undefined'){
                $bxSlider.bxSlider({
                    speed:speed*1000,
                    slideHeight:500,
                    slideWidth: width,
                    minSlides: 3,
                    maxSlides: 3,
                    slideMargin: 0,
                    moveSlides: 1,
                    auto:true,
                    adaptiveHeight: true
                });
            }
        }
    };
    main.init();

    var maps = {
        init:function(){
            initMap();
            // добавим маркер
            var xyOffice_1 =  new google.maps.LatLng(40.679812,-73.945146);
            var marker = new google.maps.Marker({
                position: xyOffice_1,
                map: map,
                title:"DLM Узел Связи"
            });

            var infoWindows =[];
            // функция закрытия всех окон
            function closeAllInfoWindows() {
                for (var i=0;i<infoWindows.length;i++) {
                    infoWindows[i].close();
                }
            }

            // создадим окно информации
            var content = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h3 class="firstHeading">Sellfing</h3>'+
                '<div id="bodyContent">'+
                '<p> 1405 Atlantic , NY 11216, USA, Brooklyn<br>Wellcome to sellfing studio<br>phone: <b> 1 (888) 88-88-888</b></p>'+
                '</div>'+
                '</div>';
            infoWindows.push(new google.maps.InfoWindow({
                content: content
            }));
            google.maps.event.addListener(marker, 'click', function() {
                closeAllInfoWindows();
                infoWindows[0].open(map,marker);
            });


        }
    };
    if(typeof $('#rootpage_map')[0]!='undefined'){
        maps.init();
    }

});
