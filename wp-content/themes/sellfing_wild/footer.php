<?php
/**
 * The Footer template
 *
 * @package WordPress
 * @subpackage Sellfing One
 * @since Sellfing One 1.0
 */
?>
		</div><!-- #main -->
	</div><!-- #page -->
<footer id="colophon" class="site-footer" role="contentinfo">
	<div id="footer_left_cont">
		<span>Phone: 1 (888) 88-88-888</span><br>
		<span>Email: sample@sample.com</span>
		<br><br>
		<span id="footer_copyright">Copyright © 2015 Sellfing Wild</span>
	</div>
	<div id="footer_right_cont">
		<p>
			<span id="footer_right_cont_title">Sellfing wild theme</span>
			<a class="footer_right_item_menu" href="?page_id=5">About us</a>
			<a class="footer_right_item_menu" href="?page_id=551">News</a>
			<a class="footer_right_item_menu" href="?page_id=12">Services</a>
			<a class="footer_right_item_menu" href="?page_id=14">Contacts</a>
		</p>
	</div>
</footer><!-- #colophon -->

	<?php wp_footer(); ?>
</body>
</html>