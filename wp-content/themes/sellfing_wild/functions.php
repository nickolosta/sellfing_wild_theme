<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 24.09.15
 * Time: 14:23
 */
function theme_setup(){
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

   if (!is_admin()){
        add_action("wp_enqueue_scripts", "my_jquery_enqueue");
   }
    function my_jquery_enqueue() {
        wp_deregister_script('jquery');
        wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js", false, null);
        wp_enqueue_script('jquery');

        if(is_page( 'Home' )){
            wp_register_script('googlemaps', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . '://maps.googleapis.com/maps/api/js?key=AIzaSyBCpytkQOgc9f3oLVq09lK6pzqZTGMYd-o');
            wp_enqueue_script('googlemaps');
        }

        if(is_page('Contact us')){
            wp_register_script('feedback',get_template_directory_uri().'/blocks/feedback.js');
            wp_localize_script('feedback', 'ajaxUrl',
                array(
                    'url' => admin_url('admin-ajax.php')
                )
            );
            wp_enqueue_script('feedback');
        }
        wp_register_script('main', get_template_directory_uri().'/js/main.js');
        wp_enqueue_script('main');
    }

}

add_action( 'after_setup_theme', 'theme_setup' );