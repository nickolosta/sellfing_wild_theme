/**
 * Created by nick on 15.10.15.
 */

jQuery(document).ready(function($){
    var feedback = {
        validate:function(){
            $('.feedback_err_show').removeClass('feedback_err_show');
            var not_error =true;
            var $name = $('#feedback_input_name');
            if($name.val().trim().length==0){
                $name.closest('.feedback_input_cont').addClass('feedback_err_show');
                not_error=false;
            }
            var $email= $('#feedback_input_email');
            if($email.val().trim().length==0){
                $email.closest('.feedback_input_cont').addClass('feedback_err_show');
                not_error=false;
            }
            var $phone = $('#feedback_input_phone');
            if($phone.val().trim().length==0){
                $phone.closest('.feedback_input_cont').addClass('feedback_err_show');
                not_error=false;
            }
            var $message =$('#feedback_input_message');
            if($message.val().trim().length==0){
                $message.closest('.feedback_input_cont').addClass('feedback_err_show');
                not_error=false;
            }
            return not_error;
        },
        events:function(){
            var $bt_send = $('#feedback_send_message');
            $bt_send.on('click',function(e){
                if(feedback.validate()){
                    var data =
                    {
                        action: 'save_feedback',
                        name:$('#feedback_input_name').val().trim(),
                        email:$('#feedback_input_email').val().trim(),
                        phone:$('#feedback_input_phone').val().trim(),
                        message:$('#feedback_input_message').val().trim()
                    };
                    $.ajax({
                        type:'POST',
                        method: "POST",
                        dataType:'json',
                        url: ajaxUrl.url,
                        data: data,
                        success:function(Data){
                            console.log(Data);
                        }
                    });
                }
            });
            $('.feedback_input').keypress(function(){
                var $ob =$(this);
                $ob.closest('.feedback_input_cont').removeClass('feedback_err_show');
            });
        },
        init:function(){
            feedback.events();
        }
    };
    feedback.init();

});