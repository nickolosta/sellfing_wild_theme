<?php
/*
Plugin Name: sellfing_news
Plugin URI: http://sellfing.com/
Version: 1.0
Author: Nick
Text Domain: sellfing
*/
define( 'SELLFING_NEWS', '1.0' );
define( 'SELLFING_NEWS__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SELLFING_NEWS__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once(SELLFING_NEWS__PLUGIN_DIR. '/class.sellfing_news.php');
require_once(SELLFING_NEWS__PLUGIN_DIR. '/class.sellfing_news.shortcodes.php');
Sellfing_news::Init();
if(is_admin())
{
    Sellfing_news::sellfing_news_Settings();
    Sellfing_news::sellfing_news_addCSS_JS();
    Sellfing_news::sellfing_news_RenderForm();
}
Sellfing_news_ShortCodes::Init();
