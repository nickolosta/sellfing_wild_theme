<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 02.10.15
 * Time: 22:51
 */
Class Sellfing_news_ShortCodes
{
    static public function Init(){

        // add shortCode  sellfing_news_last support
        function sellfing_news_last(){
            $html='';
            $args = array(
                'post_type' => 'sellfing_news',
                'orderby'   => 'date',
                'order' => 'DESC',
                'posts_per_page'=>3
            );
            $the_query = new WP_Query( $args );
            $html.='<div id="last_news_container">';
            while ( $the_query->have_posts() ) : $the_query->the_post();
                /* $post_id = get_the_ID();
                 $attachment_image = get_children( array(
                     'numberposts' => 1,
                     'post_mime_type' => 'image',
                     'post_parent' => $post_id,
                     'post_type' => 'attachment'
                 ) );
                 $attachment_image = array_shift($attachment_image);*/
                $html.='<div class="last_news_item">';
                if(get_option( 'sellfing_news_show_image' )==='1') {
                    $thumb_id = get_post_thumbnail_id();
                    $src = wp_get_attachment_image_src($thumb_id,'medium', true)[0];
                    $html .= '<a href="' . get_permalink() . '"><img  class="last_news_item_img" src="' . $src . '" /></a><br>';
                }
                $html.='<a class="last_news_item_title" href="'.get_permalink().'">'.get_the_title().'</a>';
                $html.='<p class="last_news_item_content">'.get_the_excerpt().'</p>';
                $html.='</div><!-- .last_news_item -->';
            endwhile;
            $html.='</div>';
            $html.='<hr class="last_news_item_hr"><br>';
            wp_reset_postdata();
            return $html;
        }
        add_shortcode( 'sellfing_news_last', 'sellfing_news_last' );


        function sellfing_news_list(){
            $html='';
            $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
            $args = array(
                'post_type' => 'sellfing_news',
                'orderby'   => 'date',
                'order' => 'DESC',
                'posts_per_page'=>3,
                'paged'=>$paged
            );
            $the_query = new WP_Query( $args );
            // PAGINATION //
            $big = 999999999; // need an unlikely integer
            $html.='<div id="pagination_top">';
            $html.= paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $the_query->max_num_pages
            ) );
            $html.='</div>';
            // PAGINATION //

            $html.='<div id="list_news_container">';
            while ( $the_query->have_posts() ) : $the_query->the_post();
                $thumb_id = get_post_thumbnail_id();
                $src = wp_get_attachment_image_src($thumb_id,'medium', true)[0];
                $html.='<div class="list_news_item">';
                $html.='<a href="'.get_permalink().'"><img  class="list_news_item_img" src="'.$src.'" /></a><br>';
                $html.='<a class="list_news_item_title" href="'.get_permalink().'">'.get_the_title().'</a>';
                $html.='<p class="list_news_item_content">'.get_the_excerpt().'</p>';
                $html.='</div><!-- .list_news_item -->';
                $html.='<hr class="list_news_item_hr">';
            endwhile;
            $html.='</div>';
            // PAGINATION //
            $html.='<div id="pagination_bottom">';
            $html.= paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $the_query->max_num_pages
            ) );
            $html.='</div>';
            // PAGINATION //

            return $html;
            wp_reset_postdata();
        }
        add_shortcode( 'sellfing_news_list', 'sellfing_news_list' );
    }
}