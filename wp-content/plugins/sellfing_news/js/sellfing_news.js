/**
 * Created by nick on 25.09.15.
 */
jQuery(document).ready(function($){
    console.log('!!!');
    var news ={
        modifying:function(){
        },
        init:function(){
            news.modifying();
            news.events();
        },
        validate:function(){
            var notHasErrors =true;
            var photo_file = $('#news_photo')[0].files;
            $('.sellfing_err').addClass('display_none');
            if(photo_file.length==0 && $('#news_preview').attr('src').length==0){
                $('#sellfing_news_err').removeClass('display_none');
                notHasErrors = false;
            }
            return notHasErrors;
        },
        events:function(){
            // disable default
            $('#publish').on('click',function(e){
                if(!news.validate()){
                    e.preventDefault();
                }
            });


            // object add photo
            var addPhoto = {
                init:function(){
                    var photo_file = $('#sellfing_news_photo_inp');
                    var photo = $('#sellfing_news_photo');
                    var file=null;
                    var data={};
                    $('#sellfing_news_load_bt').on('click',function(){
                        photo_file.click();
                    });

                    var reader = new FileReader();
                    var width;
                    var height;
                    var fileSize;

                    photo_file.on("change",function(){
                        file = photo_file[0].files[0];
                        reader.readAsDataURL(file);
                    });

                    reader.onloadend = function(event) {
                        var dataUri = event.target.result;
                        photo.attr('src',dataUri);
                        photo.removeClass('display_none');
                        width = photo.width;
                        height = photo.height;
                        fileSize = file.size;
                        $('#sellfing_news_photo_name').html(file.name);
                        data.photo_name = file.name;
                        data.photo = file;
                        $('#sellfing_news_err').addClass('display_none');
                    };
                    reader.onerror = function(event) {
                        console.error("File could not be read! Code " + event.target.error.code);
                    };
                }
            };
            addPhoto.init();

            var $news_bt =$('#sellfing_news_load_bt');
            $news_bt.on(function(e){
                e.stopPropagation();

            });
        }
    };
    news.init();

});