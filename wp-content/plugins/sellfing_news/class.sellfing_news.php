<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 02.10.15
 * Time: 11:32
 */
Class Sellfing_news
{
    public  static  $action ='new';
    public  static function  Helpers($helper_name,$length=0){
        switch ($helper_name) {
            case 'name_action':
                $screen = get_current_screen();
                $action = $screen->action;
                if(strlen($action)==0){
                    $action = $screen->parent_base;

                }
                return $action;
                break;
            case 'get_id':
                return get_the_ID();
                break;
            case 'random_string':
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
                break;
        }
    }
    public static  function  sellfing_news_addCSS_JS(){
        function sellfing_news_add_script(){
            if(get_post_type()=='sellfing_news'){
                wp_enqueue_style('sellfing_news_css', plugins_url( '/css/sellfing_news.css', __FILE__ ));
                wp_enqueue_script('sellfing_news_js', plugins_url( '/js/sellfing_news.js', __FILE__ ),array( 'jquery' ));
            }
        };
        add_action('admin_enqueue_scripts', 'sellfing_news_add_script');
    }
    public static function sellfing_news_Settings(){
        add_action('admin_menu', 'create_menu');
        function create_menu()
        {
            //create new top-level menu
            $page = add_submenu_page('edit.php?post_type=sellfing_news','Settings', 'Settings', 'administrator', __FILE__, 'sellfing_news_settings_page'/*,plugins_url('/images/icon.png', __FILE__)*/);
            function sellfing_news_add_settings_scripts() {
            }
            add_action('admin_print_scripts-' . $page, 'sellfing_news_add_settings_scripts');

            // create some settings in plugin
            function sellfing_news_register_mysettings()
            {
                register_setting('sellfing_news_sellfing', 'sellfing_news_show_image');
            };

            add_action('admin_init', 'sellfing_news_register_mysettings');
            function sellfing_news_settings_page()
            {
                ?>
                <div class="wrap">
                    <h2>Sellfing news settings</h2>
                    <form method="post" action="options.php">
                        <?php settings_fields('sellfing_news_sellfing');
                            $option = get_option( 'sellfing_news_show_image' );
                        ?>
                        <table class="form-table" id="sellfing_news_settings_form">
                            <tr valign="top">
                                <th scope="row">Show icon last news block</th>
                                <td>
                                    <input <?php if($option==='1') echo 'checked="checked"'?>id="sellfing_news_interval_inp" type="checkbox" name="sellfing_news_show_image" value="1"/>
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
                        </p>
                    </form>
                </div>
            <?php
            }
        }
    }
    public  static  function sellfing_news_RenderForm(){
        // add multipart form data atrribute to form
        $current_admin_page = substr(strrchr($_SERVER['PHP_SELF'], '/'), 1, -4);
        if ($current_admin_page == 'post'
            || $current_admin_page == 'post-new') {
            function add_post_enctype() {
                echo "<script type=\"text/javascript\">
        jQuery(document).ready(function(){
        jQuery('#post').attr('enctype','multipart/form-data');
        jQuery('#post').attr('encoding', 'multipart/form-data');
        });
        </script>";
            }
            add_action('admin_head', 'add_post_enctype');
        }

        function sellfing_news_photo_box() {
            $screens = array( 'sellfing_news');
            foreach ( $screens as $screen ){
                add_meta_box( 'sellfing_news_photo_container', 'Preview photo', 'render_sellfing_news_plugin', $screen,'advanced','high' );
            }
        }
        add_action('add_meta_boxes', 'sellfing_news_photo_box');

        function render_sellfing_news_plugin() {
            wp_nonce_field( plugin_basename(__FILE__), 'sellfing_news_photo__noncename' );
            $action =  Sellfing_news::Helpers('name_action');
            //echo $action;
            $postid = Sellfing_news::Helpers('get_id');
            $nameSlider='';
            if($action=='edit'){
                //get attachment img
                $attachment_image = get_children( array(
                    'numberposts' => 1,
                    'post_mime_type' => 'image',
                    'post_parent' => $postid,
                    'post_type' => 'attachment'
                ) );
                $attachment_image = array_shift($attachment_image);
                //print_r($attachment_image);
                $src = wp_get_attachment_url( $attachment_image->ID ) ;
                $nameSlider = $attachment_image->post_title;
                $img_html = '<img id="sellfing_news_photo" src="'.$src.'">';
            }else{
                $img_html = '<img id="sellfing_news_photo" src="" class="display_none">';
            }
            echo '<a id="sellfing_news_load_bt">load file</a><span id="sellfing_news_photo_name">'.$nameSlider.'</span> <span class="sellfing_err display_none" id="sellfing_news_err">required</span><br>'
                .$img_html.
                '<input class="display_none" id="sellfing_news_photo_inp"  type="file" name="sellfing_news_photo">';
        }
        /* save data */
        function sellfing_news_save_data( $post_id ) {
            if(!get_post_type()=='sellfing_news'){
                return;
            }
            if(isset($_POST)){
                if ( ! wp_verify_nonce( $_POST['sellfing_news_photo__noncename'], plugin_basename(__FILE__) ) )
                    return $post_id;
                if (!empty( $_FILES ) && strlen($_FILES['sellfing_news_photo']['name'])>0) {
                        // These files need to be included as dependencies when on the front end.
                        require_once( ABSPATH . 'wp-admin/includes/image.php' );
                        require_once( ABSPATH . 'wp-admin/includes/file.php' );
                        require_once( ABSPATH . 'wp-admin/includes/media.php' );
                        // Let WordPress handle the upload.
                        // Remember, 'my_image_upload' is the name of our file input in our form above.
                        $attachment_id = media_handle_upload( 'sellfing_news_photo',$post_id );
                        $error =  is_wp_error( $attachment_id );
                        if ($error) {
                            echo 'Errors before save post';die();
                        } else {
                            // The image was uploaded successfully!
                        }
                }else{
                    // file not uploaded, but POST data send
                }
            }
        }
        add_action( 'save_post_sellfing_news', 'sellfing_news_save_data' );

    }
    public static function Init(){
        //init plugin
        function init_sellfing_news()
        {
            $args = array(
                'labels' => array(
                    'name' => 'News',
                    'singular_name' => 'news',
                    'add_new' => 'Add news',
                    'add_new_item' => 'Add News',
                    'edit' => 'Edit',
                    'edit_item' => 'Edit news',
                    'new_item' => 'New news',
                    'view' => 'View',
                    'view_item' => 'View news',
                    'search_items' => 'Search news',
                    'not_found' => 'No news found',
                    'not_found_in_trash' => 'No news found in Trash',
                    'parent' => ''
                ),
                'supports' => array(
                    'title',
                    'editor',
                    'author',
                    'thumbnail',
                    'excerpt',
                    'comments',
                    'revisions',
                    'page-attributes',
                    'post-formats'
                ),
                'public' => true,
                'menu_position' => 14,
                'taxonomies' => array(''),
                //'menu_icon' => plugins_url( 'images/news_icon.png', __FILE__ ),
                'has_archive' => true
            );
            register_post_type('sellfing_news', $args);
        }
        add_action('init', 'init_sellfing_news');
    }
}