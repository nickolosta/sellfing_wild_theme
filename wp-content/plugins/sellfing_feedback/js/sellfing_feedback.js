/**
 * Created by nick on 16.10.15.
 */

jQuery(document).ready(function($){
    var feedback = {
        modifying:function() {
            var $title = $('#title');
            $title.attr('disabled','disabled');
        },
        init:function(){
            feedback.modifying();
            feedback.events();
        },
        validate:function(){
        },
        events:function() {
            $('#title').keypress('',function(){
                $('#sellfing_title_div_err').addClass('display_none');
            });
            // disable default
            $('#publish').on('click', function (e) {
                if (!feedback.validate()) {
                    e.preventDefault();
                }
            });
        }
    };
    feedback.init();
});