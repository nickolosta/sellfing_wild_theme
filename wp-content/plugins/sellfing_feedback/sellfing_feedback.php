<?php
/*
Plugin Name: sellfing_feedback
Plugin URI: http://sellfing.com/
Version: 1.0
Author: Nick
Text Domain: sellfing
*/
define( 'SELLFING_FEEDBACK', '1.0' );
define( 'SELLFING_FEEDBACK__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SELLFING_FEEDBACK__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once(SELLFING_FEEDBACK__PLUGIN_DIR. '/class.sellfing_feedback.php');
require_once(SELLFING_FEEDBACK__PLUGIN_DIR. '/class.sellfing_feedback.shortcodes.php');
Sellfing_feedback::Init();
if(is_admin())
{
    Sellfing_feedback::sellfing_feedback_addCSS_JS();
    Sellfing_feedback::sellfing_feedback_RenderForm();
}
    Sellfing_feedback::sellfing_feedback_Ajax();
    Sellfing_feedback_ShortCodes::Init();

