<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 15.10.15
 * Time: 12:09
 */
Class Sellfing_feedback
{
    public  static function  Helpers($helper_name,$length=0){
        switch ($helper_name) {
            case 'name_action':
                $screen = get_current_screen();
                $action = $screen->action;
                if(strlen($action)==0){
                    $action = $screen->parent_base;
                }
                return $action;
                break;
            case 'get_id':
                return get_the_ID();
                break;
            case 'random_string':
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
                break;
        }
    }
    public  static function  sellfing_feedback_Ajax(){
        // ajax callback
        add_action('wp_ajax_save_feedback', 'sellfing_feedback_ajax');
        add_action('wp_ajax_nopriv_save_feedback', 'sellfing_feedback_ajax');
        function sellfing_feedback_ajax() {
            if(!isset($_POST['name'])){
                echo '{"success":false,"error":"feedback message not save,empty row"}';
                wp_die();
            }
            $name = $_POST['name'];
            // insert new data record
            $my_post = array(
                'post_title' =>$name,
                'post_type' => 'sellfing_feedback',
                'post_status'=>'publish'
            );
            $post_id = wp_insert_post( $my_post );
            // add meta
            if(isset($_POST['phone'])){
                $phone = $_POST['phone'];
                update_post_meta( $post_id, 'sellfing_feedback_phone', $phone);
            }
            if(isset($_POST['email'])){
                $email = $_POST['email'];
                update_post_meta( $post_id, 'sellfing_feedback_email',$email );
            }
            if(isset($_POST['message'])){
                $message = $_POST['message'];
                update_post_meta( $post_id, 'sellfing_feedback_email',$email );
            }

            update_post_meta( $post_id, 'sellfing_feedback_message', $message );
            wp_die();
        }
    }
    public static  function  sellfing_feedback_settings(){

    }
    public  static  function  sellfing_feedback_addCSS_JS(){
        function sellfing_feedback_add_script(){
            $screen = get_current_screen();
            if($screen->post_type=='sellfing_feedback'){
                wp_enqueue_style('sellfing_feedback_css', plugins_url( '/css/sellfing_feedback.css', __FILE__ ));
                wp_enqueue_script('sellfing_feedback_js', plugins_url( '/js/sellfing_feedback.js', __FILE__ ),array( 'jquery' ));
            }
        };
        add_action('admin_enqueue_scripts', 'sellfing_feedback_add_script');
    }
    public  static  function  sellfing_feedback_RenderForm()
    {
        function sellfing_feedback_metabox() {
            $screens = array( 'sellfing_feedback');
            foreach ( $screens as $screen ){
                if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
                add_meta_box( 'sellfing_feedback_container', 'Feedback data', 'sellfing_feedback_render', $screen,'advanced','high' );
            }
        }
        add_action('add_meta_boxes', 'sellfing_feedback_metabox');

        function sellfing_feedback_render (){
            wp_nonce_field( plugin_basename(__FILE__), 'sellfing_feedback_nonce' );
            $action =  Sellfing_slider::Helpers('name_action');
            //echo $action;
            $post_id = Sellfing_slider::Helpers('get_id');
            $email = get_post_meta($post_id,'sellfing_feedback_email',true);
            $phone = get_post_meta($post_id,'sellfing_feedback_phone',true);
            $message = get_post_meta($post_id,'sellfing_feedback_message',true);
            echo '<div id="sellfing_feedback_cont_data">
                <b>Email:</b> '.$email.'<br>
                <b>Phone:</b> '.$phone.'<br><hr>
                <b>Message:</b><br> '.$message.'
            </div>';
        }
        
    }
    public static function  Init(){
        //init plugin
        function init_sellfing_feedback()
        {
            $args = array(
                'labels' => array(
                    'name' => 'Feedback',
                    'singular_name' => 'feedback',
                    'add_new' => 'Add feedback',
                    'add_new_item' => 'Add Feedback',
                    'edit' => 'View',
                    'view' => 'View',
                    'view_item' => 'View feedback',
                    'search_items' => 'Search feedback',
                    'not_found' => 'No feedback found',
                    'not_found_in_trash' => 'No feedback found in Trash',
                    'parent' => ''
                ),
                'supports' => array(
                    'title'
                ),
                'public' => true,
                'menu_position' => 15,
                'taxonomies' => array(''),
                //'menu_icon' => plugins_url( 'images/slider_icon.png', __FILE__ ),
                'has_archive' => true
            );
            register_post_type('sellfing_feedback', $args);
        }
        add_action('init', 'init_sellfing_feedback');
    }
}