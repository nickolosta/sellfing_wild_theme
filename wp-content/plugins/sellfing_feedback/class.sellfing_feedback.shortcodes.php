<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 15.10.15
 * Time: 12:19
 */
Class Sellfing_feedback_ShortCodes
{
    static public function Init(){
        // add shortCode
        function sellfing_feedback_shortcode(){
            $html='<div id="feedback_cont">
                <div class="feedback_cont_left">
                    <div class="feedback_input_cont">
                        <span class="feedback_err">This field is required.</span>
                         Name<br> <input placeholder="Enter name" class="feedback_input" autofocus="1" id="feedback_input_name" type="text"><br>
                    </div>
                     <div class="feedback_input_cont">
                        <span class="feedback_err">This field is required.</span>
                    Email<br> <input placeholder="Enter email" class="feedback_input" id="feedback_input_email" type="email"><br>
                    </div>
                    <div class="feedback_input_cont">
                        <span class="feedback_err">This field is required.</span>
                    Phone<br> <input placeholder="Enter phone" class="feedback_input" id="feedback_input_phone" type="text"><br>
                    </div>
                </div>
                <div class="feedback_cont_right">
                    <div class="feedback_input_cont">
                        <span class="feedback_err">This field is required.</span>
                    Message<br> <textarea class="feedback_input" placeholder="Enter message" id="feedback_input_message"></textarea>
                    </div>
                </div><br>
                <div id="feedback_send_message_cont"><input id="feedback_send_message" type="button" value="Send message"></div>
            </div>';
            return $html;
        }
        add_shortcode( 'sellfing_feedback', 'sellfing_feedback_shortcode' );
    }
}