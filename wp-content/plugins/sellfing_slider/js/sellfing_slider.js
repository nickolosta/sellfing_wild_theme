/**
 * Created by nick on 25.09.15.
 */
jQuery(document).ready(function($){
    var slider ={
        modifying:function(){
            var $divTitle =  $('#titlediv');
            $divTitle.append('<span class="sellfing_modifying_err display_none" id="sellfing_title_div_err">required</span>');

        },
        init:function(){
            slider.modifying();
            slider.events();
        },
        validate:function(){
            var notHasErrors =true;
            var photo_file = $('#slider_photo')[0].files;
            var $slider_url =$('#slider_url');
            var $title = $('#title');
            $('.sellfing_err').addClass('display_none');
            if($slider_url.val().length==0){
                $('#sellfing_slider_url_err').removeClass('display_none');
                notHasErrors = false;
            }
            if($title.val().length==0){
                $('#sellfing_title_div_err').removeClass('display_none');
                notHasErrors = false;
            }
            if(photo_file.length==0 && $('#slider_preview').attr('src').length==0){
                $('#sellfing_slider_slider_err').removeClass('display_none');
                notHasErrors = false;
            }
            return notHasErrors;
        },
        events:function(){
            // disable default
            $('#publish').on('click',function(e){
                if(!slider.validate()){
                    e.preventDefault();
                }
            });

            $('#title').keypress('',function(){
                $('#sellfing_title_div_err').addClass('display_none');
            });
            $('#slider_url').keypress(function(){
                $('#sellfing_slider_url_err').addClass('display_none');
            });

            // object add photo
            var addPhoto = {
                init:function(){
                    var photo_file = $('#slider_photo');
                    var photo = $('#slider_preview');
                    var file=null;
                    var data={};
                    $('#sellfing_slider_load_bt').on('click',function(){
                        photo_file.click();
                    });

                    var reader = new FileReader();
                    var width;
                    var height;
                    var fileSize;

                    photo_file.on("change",function(){
                        file = photo_file[0].files[0];
                        reader.readAsDataURL(file);
                    });

                    reader.onloadend = function(event) {
                        var dataUri = event.target.result;
                        photo.attr('src',dataUri);
                        photo.removeClass('display_none');
                        width = photo.width;
                        height = photo.height;
                        fileSize = file.size;
                        $('#sellfing_slider_name').html(file.name);
                        data.photo_name = file.name;
                        data.photo = file;
                        $('#sellfing_slider_slider_err').addClass('display_none');
                    };
                    reader.onerror = function(event) {
                        console.error("File could not be read! Code " + event.target.error.code);
                    };
                }
            };
            addPhoto.init();

            var $slider_bt =$('#sellfing_slider_load_bt');
            $slider_bt.on(function(e){
                e.stopPropagation();

            });
        }
    };
    slider.init();

});