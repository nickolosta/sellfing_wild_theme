/**
 * Created by nick on 30.09.15.
 */
jQuery(document).ready(function($){
    console.log(jQuery.fn.version);
    var $speed = jQuery( "#jq_slider_range" );
    var $interval = jQuery( "#jq_slider_range_iterval" );
    var $speedInp =jQuery( "#sellfing_slider_speed_inp" );
    var $intervalInp =jQuery( "#sellfing_slider_interval_inp" );
    var $speedLabel =jQuery('#sellfing_slider_speed_label');
    var $intervalLabel =jQuery('#sellfing_slider_interval_label');
    $speed.slider({
        range: "max",
        min: 1,
        max: 5,
        value: 1,
        slide: function( event, ui ) {
            $speedInp.val( ui.value );
            $speedLabel.text(ui.value);
        }
    });
    $interval.slider({
        range: "max",
        min: 1,
        max: 5,
        value: 1,
        slide: function( event, ui ) {
            jQuery( "#sellfing_slider_interval_inp" ).val( ui.value );
            $intervalLabel.text(ui.value);
        }
    });
    $speed.slider( "value",$speedInp.val());
    $interval.slider( "value",$intervalInp.val());
    $speedLabel.text($speedInp.val());
    $intervalLabel.text($intervalInp.val());
});