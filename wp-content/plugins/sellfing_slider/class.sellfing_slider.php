<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 18.09.15
 * Time: 19:35
 */
Class Sellfing_slider
{
    public  static  $action ='new';
    public  static function  Helpers($helper_name,$length=0){
        switch ($helper_name) {
            case 'name_action':
                    $screen = get_current_screen();
                    $action = $screen->action;
                    if(strlen($action)==0){
                        $action = $screen->parent_base;

                    }
                    return $action;
            break;
            case 'get_id':
                 return get_the_ID();
            break;
            case 'random_string':
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return $randomString;
                break;
        }
    }
    public static  function  addCSS_JS(){
        function add_script(){
            if(get_post_type()=='slider'){
                wp_enqueue_style('sellfing_slider_css', plugins_url( '/css/sellfing_slider.css', __FILE__ ));
                wp_enqueue_script('sellfing_slider_js', plugins_url( '/js/sellfing_slider.js', __FILE__ ),array( 'jquery' ));
            }
        };
        add_action('admin_enqueue_scripts', 'add_script');
    }
    public static  function  RenderForm(){
        function init_render_sellfing_slider(){
           // echo get_post_type(); die();
            // add multipart form data atrribute to form
            $current_admin_page = substr(strrchr($_SERVER['PHP_SELF'], '/'), 1, -4);
            if ($current_admin_page == 'post' || $current_admin_page == 'post-new') {
                if(!function_exists('add_post_enctype')){
                    function add_post_enctype()
                    {
                        echo "<script type=\"text/javascript\">
                        jQuery(document).ready(function(){
                        jQuery('#post').attr('enctype','multipart/form-data');
                        jQuery('#post').attr('encoding', 'multipart/form-data');
                        });
                        </script>";
                    }
                    add_action('admin_head', 'add_post_enctype');
                }
            }

            function sellfing_slider_image() {
                $screens = array( 'slider');
                foreach ( $screens as $screen )
                    add_meta_box( 'slider', 'Slider image', 'render_sellfing_slider', $screen );
            }
            add_action('add_meta_boxes', 'sellfing_slider_image');

            function render_sellfing_slider() {
                wp_nonce_field( plugin_basename(__FILE__), 'sellfing_slider_nonce' );
                $action =  Sellfing_slider::Helpers('name_action');
                //echo $action;
                $post_id = Sellfing_slider::Helpers('get_id');
                $nameSlider='';
                $url='';
                if($action=='edit'){
                    $url=get_post_meta($post_id,'slider_url',true);
                    //print_r($url);die();
                    $src = plugin_dir_url(__FILE__).get_post_meta($post_id,'slider',true);
                    $nameSlider = get_post_meta($post_id,'slider_name',true);
                    $img_html = '<img id="slider_preview" src="'.$src.'">';
                }else{
                    $img_html = '<img id="slider_preview" src="" class="display_none">';
                }
                echo 'Url <input type="text" value="'.$url.'" name="slider_url" id="slider_url"><span class="sellfing_err display_none" id="sellfing_slider_url_err">required</span><br>
                <a id="sellfing_slider_load_bt">load file</a><span id="sellfing_slider_name">'.$nameSlider.'</span> <span class="sellfing_err display_none" id="sellfing_slider_slider_err">required</span><br>'
                    .$img_html.
                    '<input class="display_none" id="slider_photo"  type="file" name="slider_photo">';
            }

            /* save data */
            function sellfing_slider_save_data( $post_id ) {
                if(!get_post_type()=='slider'){
                    return;
                }
                if ( ! wp_verify_nonce( $_POST['sellfing_slider_nonce'], plugin_basename(__FILE__) ) )
                    return $post_id;

                if(isset($_POST)){
                    if (!empty( $_FILES ) && strlen($_FILES['slider_photo']['name'])>0) {
                        $file = $_FILES['slider_photo'];
                        $fileName = $file['name'];
                        // TODO
                        $file = $_FILES['slider_photo'];
                        $fileName = $file['name'];
                        $ext  = substr($fileName, strrpos($fileName, '.'));
                        $dir =  plugin_dir_path( __FILE__ ).'uploads/';
                        $newName =Sellfing_slider::Helpers('random_string',20).$ext;
                        //$my_data = sanitize_text_field($fileName);
                        if (move_uploaded_file($file['tmp_name'],$dir.$newName)) {
                            // print_r($file['tmp_name']);die();
                            // set file rights
                            chmod($dir.$newName,0644);
                            // update data
                            update_post_meta( $post_id, 'slider_url', $_POST['slider_url'] );
                            update_post_meta( $post_id, 'slider_name', $fileName );
                            update_post_meta( $post_id, 'slider',  'uploads/'.$newName );
                        }
                        //  die();
                    }else{
                        update_post_meta( $post_id, 'slider_url', $_POST['slider_url'] );
                    }
                }
            }
            add_action( 'save_post_slider', 'sellfing_slider_save_data' );
        }
        add_action('init', 'init_render_sellfing_slider');
    }
    public  static  function  Settings(){
        // set folder rights for plugin
        chmod( plugin_dir_path( __FILE__ ).'uploads/', 755);
        // create  plugin settings menu
        add_action('admin_menu', 'sellfing_news_create_menu');
        function sellfing_news_create_menu()
        {
            //create new top-level menu
            $page = add_submenu_page('edit.php?post_type=slider','Settings', 'Settings', 'administrator', __FILE__, 'sellfing_slider_settings_page'/*,plugins_url('/images/icon.png', __FILE__)*/);
            function add_settings_scripts() {
                wp_enqueue_style('sellfing_slider_cs_settings', plugins_url( '/css/sellfing_slider_settings.css', __FILE__ ));
                wp_register_script('jquery-ui_css','http://code.jquery.com/ui/1.11.4/themes/excite-bike/jquery-ui.css');
                wp_enqueue_style('jquery-ui_css','http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
                wp_register_script("jquery-ui",'http://code.jquery.com/ui/1.11.4/jquery-ui.js', array('jquery'));
                wp_enqueue_script( 'jquery-ui');
                wp_enqueue_script('sellfing_slider_settings', plugins_url( '/js/sellfing_slider_settings.js', __FILE__ ),array( 'jquery' ));
            }
            add_action('admin_print_scripts-' . $page, 'add_settings_scripts');

            // create some settings in plugin
            function register_mysettings()
            {
                register_setting('slider_sellfing', 'speed');
                register_setting('slider_sellfing', 'interval');
            }

            ;
            add_action('admin_init', 'register_mysettings');

            function sellfing_slider_settings_page()
            {
                ?>
                <div class="wrap">
                    <h2>Sellfing slider settings</h2>
                    <form method="post" action="options.php">
                        <?php settings_fields('slider_sellfing'); ?>
                        <table class="form-table" id="sellfing_slider_settings_form">
                            <tr valign="top">
                                <th scope="row">Speed = <span id="sellfing_slider_speed_label"></span> sec</th>
                                <td>
                                    <input id="sellfing_slider_speed_inp" type="text" name="speed" value="<?php echo get_option('speed'); ?>"/>
                                    <div id="jq_slider_range">

                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th scope="row">Interval =  <span id="sellfing_slider_interval_label"></span> sec</th>
                                <td>
                                    <input id="sellfing_slider_interval_inp" type="text" name="interval" value="<?php echo get_option('interval'); ?>"/>
                                    <div id="jq_slider_range_iterval">

                                    </div>
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
                        </p>
                    </form>
                </div>
            <?php
            }
        }
    }
    public static function  Init()
    {
        //init plugin
        function init_slider()
        {
            $args = array(
                'labels' => array(
                    'name' => 'Sliders',
                    'singular_name' => 'slider',
                    'add_new' => 'New slider',
                    'add_new_item' => 'Add New slider',
                    'edit' => 'Edit',
                    'edit_item' => 'Edit slider',
                    'new_item' => 'New slider',
                    'view' => 'View',
                    'view_item' => 'View slider',
                    'search_items' => 'Search Movie Reviews',
                    'not_found' => 'No slider found',
                    'not_found_in_trash' => 'No slider found in Trash',
                    'parent' => ''
                ),
                'supports' => array(
                    'title',
                    'thumbnail'
                ),
                'public' => true,
                'menu_position' => 15,
                'taxonomies' => array(''),
                //'menu_icon' => plugins_url( 'images/slider_icon.png', __FILE__ ),
                'has_archive' => false
            );
            register_post_type('slider', $args);
        }
        add_action('init', 'init_slider');

        // modifying list form
        add_filter('manage_slider_posts_columns', 'bs_event_table_head');
        function bs_event_table_head( $defaults ) {
            $defaults['preview']  = __( 'Slide', 'slide_preview_column' );
            return $defaults;
        }

        add_action( 'manage_slider_posts_custom_column', 'bs_slider_table_content', 10, 2 );
        function bs_slider_table_content( $column_name, $post_id ) {
            if ($column_name == 'preview') {
                $src = plugin_dir_url(__FILE__).get_post_meta($post_id,'slider',true);
                echo '<img class="slider_view_preview" src="'.$src.'"';
            }
        }
    }
}
?>