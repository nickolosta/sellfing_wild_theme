<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 02.10.15
 * Time: 23:00
 */

Class Sellfing_slider_ShortCodes
{
   static  public  function Init(){
           // register all scripts for shortcodes
           add_action('init', 'register_my_script');
           add_action('wp_footer', 'print_my_script');
           function register_my_script() {
               wp_register_style( 'bxslider_style',plugins_url( '/css/jquery.bxslider.css' , __FILE__ ));
               wp_register_script( 'bxslider', plugins_url( '/js/jquery.bxslider.min.js' , __FILE__ ));
           }

           function print_my_script() {
               global $add_sellfing_slider_scripts;

               if ( ! $add_sellfing_slider_scripts )
                   return;
               wp_enqueue_style( 'bxslider_style' );
               wp_enqueue_script( 'bxslider' );
           }

    // add shortCode sellfing slider support
           function sellfing_slider(){
               global $add_sellfing_slider_scripts;
               $add_sellfing_slider_scripts = true;

               $html='';
               $args = array( 'post_type' => 'slider');
               $the_query = new WP_Query( $args );
               $html.='<div class="slider_container"><ul class="bxslider" data-interval="'.get_option('interval').'" data-speed="'.get_option('speed').'">';

               while ( $the_query->have_posts() ) : $the_query->the_post();
                   $post_id = get_the_ID();
                   $html.='<li><img src="'.plugin_dir_url(__FILE__).get_post_meta($post_id,'slider',true).'" /></li>';
               endwhile;
               $html.='</ul></div>';
               wp_reset_postdata();
               return $html;
           }
           add_shortcode( 'sellfing_slider', 'sellfing_slider' );
   }
}