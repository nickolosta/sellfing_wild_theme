<?php
/*
Plugin Name: sellfing_slider
Plugin URI: http://sellfing.com/
Version: 1.0
Author: Nick
Text Domain: sellfing
*/
define( 'SSLIDER_VERSION', '1.0' );
define( 'SSLIDER__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SSLIDER__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once(SSLIDER__PLUGIN_DIR. '/class.sellfing_slider.php');
require_once(SSLIDER__PLUGIN_DIR. '/class.sellfing_slider.shortcodes.php');
Sellfing_slider::Init();
if(is_admin())
{
    Sellfing_slider::Settings();
    Sellfing_slider::addCSS_JS();
    Sellfing_slider::RenderForm();
}
Sellfing_slider_ShortCodes::Init();


