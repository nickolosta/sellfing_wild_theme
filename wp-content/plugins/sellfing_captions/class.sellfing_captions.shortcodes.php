<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 06.10.15
 * Time: 23:06
 */

Class Sellfing_captions_ShortCodes
{
    static  public  function Init(){
        // add shortCode sellfing slider support
        function sellfing_captions_shortcode($atr){
            $atr = shortcode_atts(
                array(
                    'post_id' => null,
                ), $atr, 'sellfing_captions' );
            $html='';
            $args = array( 'post_type' => 'sellfing_captions','p'=>$atr['post_id'],'posts_per_page' => 1);
            $the_query = new WP_Query( $args );
            $html.='<div class="captions_container">';
                $html.='<div class="captions_content">';
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    $html.='<h3 class="captions_title">'.get_the_title().'</h3>';
                    $html.='<p class="captions_small_text">'. get_post_meta($atr['post_id'],'caption_text_small',true).'</p>';
                    //$html.='<p class="captions_small_link"><a href="'.get_post_meta($atr['post_id'],'caption_link_url',true).'">'.get_post_meta($atr['post_id'],'caption_link_text',true).'</a></p>';
                endwhile;
                $html.='</div>';
            $html.='</div>';
            wp_reset_postdata();
            return $html;
        }
        add_shortcode( 'sellfing_captions', 'sellfing_captions_shortcode' );
    }
}