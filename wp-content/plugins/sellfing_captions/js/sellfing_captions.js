/**
 * Created by nick on 03.10.15.
 */
jQuery(document).ready(function($){
    var captions = {
        modifying:function(){
            var $divTitle =  $('#titlediv');
            $divTitle.append('<span class="caption_err display_none" id="sellfing_title_div_err">required</span>');
        },
        init:function(){
            captions.modifying();
            captions.events();
        },
        validate:function(){
            var notHasErrors =true;
            var $caption_text =$('#title');
            $('.caption_err').addClass('display_none');
            if($caption_text.val().length==0){
                $('#sellfing_title_div_err').removeClass('display_none');
                notHasErrors=false;
            }
            return notHasErrors;
        },
        events:function() {
            $('#title').keypress('',function(){
                $('#sellfing_title_div_err').addClass('display_none');
            });
            // disable default
            $('#publish').on('click', function (e) {
                if (!captions.validate()) {
                    e.preventDefault();
               }
            });
        }
    };
    captions.init();
});