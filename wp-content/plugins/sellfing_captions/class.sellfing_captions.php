<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 02.10.15
 * Time: 11:32
 */
Class Sellfing_captions
{
    public  static  $action ='new';
    public  static function  Helpers($helper_name,$length=0){
        switch ($helper_name) {
            case 'name_action':
                $screen = get_current_screen();
                $action = $screen->action;
                if(strlen($action)==0){
                    $action = $screen->parent_base;

                }
                return $action;
                break;
            case 'get_id':
                return get_the_ID();
                break;
            case 'random_string':
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
                break;
        }
    }
    public static  function  sellfing_captions_addCSS_JS(){
        function sellfing_captions_add_script(){
            if(get_post_type()=='sellfing_captions'){
                wp_enqueue_style('sellfing_captions_css', plugins_url( '/css/sellfing_captions.css', __FILE__ ));
                wp_enqueue_script('sellfing_captions_js', plugins_url( '/js/sellfing_captions.js', __FILE__ ),array( 'jquery' ));
            }
        };
        add_action('admin_enqueue_scripts', 'sellfing_captions_add_script');
    }
    public  static  function sellfing_captions_RenderForm(){
        function sellfing_captions_boxes() {
            $screens = array( 'sellfing_captions');
            foreach ( $screens as $screen ){
                add_meta_box( 'sellfing_captions_container', 'Captions', 'render_sellfing_captions_plugin', $screen,'advanced','high' );
            }
        }
        add_action('add_meta_boxes', 'sellfing_captions_boxes');

        function render_sellfing_captions_plugin() {
            wp_nonce_field( plugin_basename(__FILE__), 'sellfing_captions_photo__noncename' );
            $action =  Sellfing_captions::Helpers('name_action');
            //echo $action;
            $postid = Sellfing_captions::Helpers('get_id');
            if($action=='edit'){
                $caption_text=get_post_meta($postid,'caption_text',true);
                $caption_text_small =get_post_meta($postid,'caption_text_small',true);
                $caption_link_text =get_post_meta($postid,'caption_link_text',true);
                $caption_link_url =get_post_meta($postid,'caption_link_url',true);
            }else{
                $caption_text='';
                $caption_text_small ='';
                $caption_link_text ='';
                $caption_link_url ='';
            }
            echo '<div id="captions_container">
                <span class="caption_link_label">Text small</span><textarea value="'.$caption_text_small.'" name="caption_text_small" id="caption_text_small">'.$caption_text_small.'</textarea><span id="caption_text_err" class="caption_err display_none">required</span><br>
                <span class="caption_link_label">link</span><input value="'.$caption_link_text.'" id="caption_link_text" name="caption_link_text"><span id="caption_text_err" class="caption_err display_none">required</span><br>
                <span class="caption_link_label">link Url</span><input value="'.$caption_link_url.'" id="caption_link_url" name="caption_link_url"><span id="caption_text_err" class="caption_err display_none">required</span>
            </div>';
        }

        /* save data */
        function sellfing_captions_save_data( $post_id ) {
            if(!get_post_type()=='sellfing_captions'){
                return;
            }
            if(isset($_POST)){
                update_post_meta( $post_id, 'caption_text_small', $_POST['caption_text_small'] );
                update_post_meta( $post_id, 'caption_link_text', $_POST['caption_link_text'] );
                update_post_meta( $post_id, 'caption_link_url', $_POST['caption_link_url'] );
            }
        }
        add_action( 'save_post_sellfing_captions', 'sellfing_captions_save_data' );

    }
    public static function Init(){
        //init plugin
        function init_sellfing_captions()
        {
            $args = array(
                'labels' => array(
                    'name' => 'Captions',
                    'singular_name' => 'caption',
                    'add_new' => 'Add caption',
                    'add_new_item' => 'Add caption',
                    'edit' => 'Edit',
                    'edit_item' => 'Edit caption',
                    'new_item' => 'New caption',
                    'view' => 'View',
                    'view_item' => 'View caption',
                    'search_items' => 'Search caption',
                    'not_found' => 'No captions found',
                    'not_found_in_trash' => 'No captions found in Trash',
                    'parent' => ''
                ),
                'supports' => array(
                    'title'
                ),
                'public' => true,
                'menu_position' => 13,
                'taxonomies' => array(''),
                //'menu_icon' => plugins_url( 'images/slider_icon.png', __FILE__ ),
                'has_archive' => true
            );
            register_post_type('sellfing_captions', $args);

            // modifying list form
            add_filter('manage_sellfing_captions_posts_columns', 'sellfing_captions_table_head');
            function sellfing_captions_table_head( $defaults ) {
                $defaults['post_id']  = __( 'ID', 'post_id' );
                $defaults['text_small']  = __( 'Text small', 'text_small' );
                $defaults['caption_link_text']  = __( 'Link', 'caption_link_text' );
                $defaults['caption_link_url']  = __( 'Link url', 'caption_link_url' );
                return $defaults;
            }

            add_action( 'manage_sellfing_captions_posts_custom_column', 'sellfing_captions_table_content', 10, 2 );
            function sellfing_captions_table_content( $column_name, $post_id ) {
                if ($column_name == 'post_id') {
                    $text =$post_id;
                    echo $text;
                }
                if ($column_name == 'text_small') {
                    $text = get_post_meta($post_id,'caption_text_small',true);
                    echo $text;
                }
                if ($column_name == 'caption_link_text') {
                    $text = get_post_meta($post_id,'caption_link_text',true);
                    echo $text;
                }
                if ($column_name == 'caption_link_url') {
                    $text = get_post_meta($post_id,'caption_link_url',true);
                    echo $text;
                }
            }
        }
        add_action('init', 'init_sellfing_captions');
    }
}