<?php
/*
Plugin Name: sellfing_captions
Plugin URI: http://sellfing.com/
Version: 1.0
Author: Nick
Text Domain: sellfing
*/
define( 'SELLFING_CAPTIONS', '1.0' );
define( 'SELLFING_CAPTIONS__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SELLFING_CAPTIONS__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once(SELLFING_CAPTIONS__PLUGIN_DIR. '/class.sellfing_captions.php');
require_once(SELLFING_CAPTIONS__PLUGIN_DIR. '/class.sellfing_captions.shortcodes.php');
Sellfing_captions::Init();
if(is_admin())
{
   // Sellfing_news::Settings();
    Sellfing_captions::sellfing_captions_addCSS_JS();
    Sellfing_captions::sellfing_captions_RenderForm();
}
Sellfing_captions_ShortCodes::Init();
